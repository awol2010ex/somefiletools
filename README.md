实现方法:

1) 是否二进制文件 （true 是 , false 否）

   pub fn is_binary_file(file_path :&str) -> Option<bool>

   测试:  cargo run  --example test_is_binary_file

2) 抓取pdf内的文本   

   pub fn fetch_pdf_text(file_path :&str ) -> Result<String ,pdf_extract::OutputError>

   测试:  
   
   yum -y install fontconfig
   
   mkdir -p /usr/share/fonts/chinese

   yum -y install ttmkfdir
   
   ttmkfdir -e /usr/share/X11/fonts/encodings/encodings.dir

   cargo run  --example test_fetch_pdf_text
   



