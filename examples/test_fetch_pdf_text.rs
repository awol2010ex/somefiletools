extern crate filetools;
use std::env;
fn main() {
    //当前目录
    let current_dir = env::current_dir().unwrap();
    let current_dir_path = current_dir.to_str().unwrap();
    //测试pdf
    let test_pdf_path = format!("{}/examples/第二章：Flink部署与应用.pdf", &current_dir_path);
    println!(
        "{}",
        filetools::fetch_pdf_text(&test_pdf_path).unwrap()
    );
}
