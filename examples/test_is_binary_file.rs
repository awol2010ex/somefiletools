extern crate filetools;
use std::env;
fn main() {
    //当前目录
    let current_dir = env::current_dir().unwrap();
    let current_dir_path = current_dir.to_str().unwrap();
    //文本文件判断
    let test_file1_path = format!("{}/examples/test_file1.txt", &current_dir_path);
    println!(
        "test_file1 is binary file:{}",
        filetools::is_binary_file(&test_file1_path).unwrap()
    );

    //二进制文件判断
    let test_file2_path = format!("{}/examples/test_file2.jpg", &current_dir_path);
    println!(
        "test_file2 is binary file:{}",
        filetools::is_binary_file(&test_file2_path).unwrap()
    );
}
