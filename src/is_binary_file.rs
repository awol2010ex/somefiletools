use std::fs::File;
use std::io::Read;
use std::path::Path;
const CHUNKSIZE: usize = 8192;
/*判断是文本文件还是二进制文件  ，true 文本文件 false 二进制文件*/
pub fn is_binary_file(file_path: &str) -> Option<bool> {
    let mut read_size = CHUNKSIZE;
    let path = Path::new(&file_path);
    let file_size = path.metadata().unwrap().len() as usize;
    if read_size > file_size {
        read_size = file_size;
    }

    let mut buffer = [0; CHUNKSIZE];
    let mut _inputfile = File::open(&file_path).unwrap();
    _inputfile.read(&mut buffer).unwrap();
    //println!("buffer={:?}",buffer);

    let mut i = 0;
    while i < read_size {
        if buffer[i] == 0 {
            return Some(true);
        }

        i = i + 1;
    }

    Some(false)
}
