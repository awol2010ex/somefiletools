extern crate pdf_extract;
/*抓取pdf内的文本*/
pub fn fetch_pdf_text(file_path :&str ) -> Result<String ,pdf_extract::OutputError> {
    pdf_extract::extract_text(&file_path)
}