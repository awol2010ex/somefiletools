mod is_binary_file;
mod fetch_pdf_text;
/*判断是文本文件还是二进制文件  ，true 文本文件 false 二进制文件*/
pub use is_binary_file::is_binary_file as is_binary_file;

/*抓取pdf内的文本*/
pub use fetch_pdf_text::fetch_pdf_text as fetch_pdf_text;